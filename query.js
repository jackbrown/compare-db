
var mysql = require('mysql');
function connection({host,username,password,db}){
	var con = mysql.createConnection({
	  host: host,
	  user: username,
	  password: password,
	  database: db
	});
	function queryRaw(query){
		function execQuery(resolve,reject,query){
			con.query(query,  function (err, result, fields) {
				   if (err){
					   reject(err)
				   }else{
					  resolve(result);
				   }
			   })
		}
		return new Promise(function(resolve,reject){
			if (con.state=="authenticated"){
				execQuery(resolve,reject,query);
			}else{
				con.connect(async function(err) {
				  if (err) {
					 reject(err)
				  }else{
					execQuery(resolve,reject,query);
				  }
			  })
			}
		})
	}
	return queryRaw;
}
exports.connection=connection
