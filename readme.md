
# comparedb

Para comparar dos bases de datos en mysql.

## Instalación
``` sh
npm install -g comparedb
```
## Modo de uso
``` sh
comparedb <user>:<password>@<host>:<dbA> <user>:<password>@<host>:<dbB>
```
Ejemplo:
``` sh
comparedb root:toor@localhost:prueba root:toor@localhost:prueba1
```
# Resultado:
- Un json que contiene un listado de las tablas que son iguales en ambas dbs.
- Un listado de las tablas que pertenecen solo a la dbA
- Un listado de las tablas que pertenecen solo a la dbB
- Un listado de las tablas que pertenecen a ambas dbs, y son diferentes
    la diferencia entre estas tablas viene dada por
    - Un listado de atributos que existe en la dbA solamente
    - Un listado de atributos que existe en la dbB solamente
    - Un listado de atributos comunues, que tienen alguna propiedad diferente en ambas dbs (por ejemplo el tipo de dato, o el defaultValue)

Ejemplo de resultado:
``` json
{
  "DBA": "prueba",
  "DBB": "prueba1",
  "tablesInBothDB": {
    "equals": [
      "Persona"
    ],
    "different": [
      {
        "tableName": "Mascota",
        "attributesOnlyInDBA": ["raza"],
        "attributesOnlyInDBB": ["color"],
        "inBothButDifferent": [
          {
            "nombre": {
              "Type": {
                "eon": "varchar(255)",
                "eonup": "longtext"
              }
            }
          }
        ]
      }
    ]
  },
  "tablesOnlyInDBA": [
    "TablaDos"
  ],
  "tablesOnlyInDBB": []
}
```


## Parametros extras (agregarlos al final): 
 
 - -e para evitar mostrar las tablas que son iguales en ambas dbs.
 - -d para evitar mostrar los atributos que se encuentran en ambas tablas pero son diferentes.
