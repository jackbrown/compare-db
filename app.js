#!/usr/bin/env node
if (process.argv.length<3){
	var fs=require("fs");
	var data=fs.readFileSync("readme.md",'utf8');
	console.log(data);
	process.exit(0);
}
var AA=process.argv[2];
var BB=process.argv[3];
var dbA="";
var dbB="";
var showISaveEquals=true;
var inBothButDifferent=true;
if (process.argv.indexOf("-e")!=-1){
	showISaveEquals=false;
}
if (process.argv.indexOf("-d")!=-1){
	inBothButDifferent=false;
}
function getInfo(X){
	X=X.split("@")
	X[0]=X[0].split(":")
	X[1]=X[1].split(":");
	if (dbA==""){
		dbA=X[1][1]
	}else{
		dbB=X[1][1]
	}
	return {
		host: X[1][0],
		db: X[1][1],
		username: X[0][0],
		password: X[0][1]
	}
}

var query= require("./query").connection(getInfo(AA));
var query2= require("./query").connection(getInfo(BB));

function proc(infoA,infoB, db1, db2, arr1, both){
	for (var a=0;a<infoA.length;a++){
		var t=infoA[a]["Tables_in_"+db1];
		var wasFound=false;
		for(var b=0;b<infoB.length && !wasFound;b++){ 
			if (t==infoB[b]["Tables_in_"+db2]){
				wasFound=true;
			}
		}
		if (wasFound){
			if (both.indexOf(t)==-1){
				both.push(t);
			}
		}
		else{
			arr1.push(t)
		}
	}
}

function compareArrays(infoA,infoB){
	var asT=[];
	var bsT=[];
	var bothT=[];
	proc(infoA,infoB,dbA,dbB,asT,bothT);
	proc(infoB,infoA,dbB,dbA,bsT,bothT);
	return {
		A: asT,
		B: bsT,
		both: bothT
	}
	
}

function procAttr(dbA,dbB, tA,tB,aa,ab,c,needSave){
	for (var i=0;i<tA.length;i++){
		var t=tA[i];
		var currentField={}
		var wasFound=false;
		var found={};
		for (var j=0;j<tB.length&& !wasFound;j++){
			if (t.Field==tB[j].Field){
				wasFound=true;
				found=tB[j];
			}
		}
		if (wasFound ){
			if (needSave){
				var isDiferent=false;
				if (found.Type!=t.Type){
					isDiferent=true;
					currentField["Type"]={
						[dbA]: t.Type,
						[dbB]: found.Type,
					}
				}
				if (found.Null!=t.Null){
					isDiferent=true;
					currentField["Null"]={
						[dbA]: t.Null,
						[dbB]: found.Null,
					}
				}
				if (found.Key!=t.Key){
					isDiferent=true;
					currentField["Null"]={
						[dbA]: t.Key,
						[dbB]: found.Key,
					}
				}
				if (found.Default!=t.Default){
					isDiferent=true;
					currentField["Default"]={
						[dbA]: t.Default,
						[dbB]: found.Default,
					}
				}
				if (found.Extra!=t.Extra){
					isDiferent=true;
					currentField["Extra"]={
						[dbA]: t.Extra,
						[dbB]: found.Extra,
					}
				}
				if (isDiferent){
					c.push({
						[t.Field]: currentField
						});
				}
			}
		}else{
			aa.push(t)
		}
	 }
}

function compareTables(tableName,dbA,dbB,tableA,tableB){
	/**
	 *  [
	 *  	RowDataPacket {
				Field: 'id',
				Type: 'int',
				Null: 'NO',
				Key: 'PRI',
				Default: null,
				Extra: 'auto_increment'
			 }
		]
	 * */
	var a=[];
	var b=[];
	var c=[];
	procAttr(dbA, dbB, tableA,tableB,a,b,c,true);
	procAttr(dbB, dbA, tableB,tableA,b,a,c,false);
	var result={
		tableName: tableName,
		["attributesOnlyInDBA"]: a,
		["attributesOnlyInDBB"]: b,
		inBothButDifferent: c	
	}
	if (!inBothButDifferent){
		delete result.inBothButDifferent;
	}
	return result;
}

function showCreateTable(queryX,dbX, tableName){
	return queryX("show columns from `"+tableName+"`");
}

var finalResult={
	DBA: dbA,
	DBB: dbB,
	"tablesInBothDB": {
		equals: [],
		different: []
	}
}
query("show tables;").then(function(infoA){
	query2("show tables;").then(async function(infoB){
		var comparation=compareArrays(infoA,infoB);
		finalResult["tablesOnlyInDBA"]=comparation.A;
		finalResult["tablesOnlyInDBB"]=comparation.B;
		for(var i=0;i<comparation.both.length;i++){
			var tableA= await showCreateTable(query,dbA,comparation.both[i]);
			var tableB= await showCreateTable(query2,dbB,comparation.both[i]);
			if (JSON.stringify(tableA)!=JSON.stringify(tableB)){
				var rx=compareTables(comparation.both[i],dbA,dbB,tableA,tableB);
				finalResult.tablesInBothDB.different.push(rx)
			}else {
				finalResult.tablesInBothDB.equals.push(comparation.both[i])
			}
		}
		if (!showISaveEquals){
			console.log("Borrando las iguales")
			delete finalResult.tablesInBothDB.equals;
		}
		console.log(JSON.stringify(finalResult,null,2));
		process.exit(0)
	})
})
